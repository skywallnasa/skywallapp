package io.skywallapp.skywall;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vonziper on 15-04-12.
 */
public class Drawing {

    public int width = 0;
    public int height = 0;
    public double lat;
    public double lng;
    public double yaw;
    public double pitch;
    public double roll;
 public double alt;
    Paint paint = new Paint();


    public List<Point> points = new ArrayList<>();

    Drawing() {
        paint.setColor(Color.RED);
    }

    public JSONObject toJson() throws JSONException {

        JSONObject drawingObj = new JSONObject();

        drawingObj.put("width", width);
        drawingObj.put("height", height);

        JSONArray ptsArr = new JSONArray();

        for (Point point : this.points) {
            ptsArr.put(point.toJson());
        }

        // Positionnnement
        drawingObj.put("lat", lat);
        drawingObj.put("lng", lng);
        drawingObj.put("yaw", yaw);
        drawingObj.put("pitch", pitch);
        drawingObj.put("roll", roll);

        // Points
        drawingObj.put("points", ptsArr);

        return drawingObj;
    }

    public void onDraw(Canvas canvas) {

        // for each point, draw on canvas
        for (Point point : points)
        {
            point.draw(canvas, paint);
        }
    }



    /**
     * Envoie du messeage au serveur.
     * Format :
     *   width: req.body.width,
         height: req.body.height,
         points: req.body.points,
         lat: req.body.lat,
         lng: req.body.lng,
         yaw: req.body.yaw,
         pitch: req.body.pitch,
         roll: req.body.roll
     *
     */
    public void sendToServer() {

        Thread thread = new Thread() {
            @Override
            public void run()
            {
                String url = "http://104.236.226.140/message";

        try {
            String urlParameters = toJson().toString();
            String response = RequestManager.excutePost(url, urlParameters, true);

            System.out.print(response);

                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        };
        thread.start();
    }

}
