package io.skywallapp.skywall;


import android.graphics.Color;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vonziper on 15-04-11.
 */
public class RequestManager implements Runnable{

    public static String excutePost(String targetURL, String urlParameters, boolean post)
    {
        URL url;
        HttpURLConnection connection = null;
        try {
            // Create HTTP connection
            url = new URL(targetURL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod(post ? "POST" : "GET");
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setRequestProperty("Content-Length", "" +
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // Send request to server
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream ());
            wr.writeBytes (urlParameters);
            wr.flush ();
            wr.close ();

            // Get response from server
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            return response.toString();

        } catch (Exception e) {

            e.printStackTrace();
            return null;

        } finally {

            if(connection != null) {
                connection.disconnect();
            }
        }
    }

/*
    byte mImage[] = null;
    void setData(byte image[])
    {
        mImage = image;
    }*/


    void sendImage(byte[] data, final String targetURL, final double lat, final double lng, final double yaw, final double pitch, final double roll)
    {
        final byte[] image = data;
        Thread thread = new Thread()
        {
            @Override
            public void run()
            {


                try
                {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(targetURL);
                    httpPost.setEntity(new ByteArrayEntity(image));
                    httpPost.addHeader("lat", ""+lat);
                    httpPost.addHeader("lng", ""+lng);
                    httpPost.addHeader("yaw", ""+yaw);
                    httpPost.addHeader("pitch", ""+pitch);
                    httpPost.addHeader("roll", ""+roll);


                    HttpResponse response = httpClient.execute(httpPost);
                    System.out.println(EntityUtils.toString(response.getEntity()));
                    Log.d("SendThread", "Image sended");


                }
                catch(ClientProtocolException exception)
                {
                    System.out.println(exception.getMessage());

                }
                catch(IOException exception)
                {
                    System.out.println(exception.getMessage());
                }


                Log.d("SendThread", "End");

            }
        };

        thread.start();
    }

    /*static void getMessage() {

        final String targetURL = "http://104.236.226.140/messages";

        Thread thread = new Thread()
        {
            @Override
            public void run()
            {

                Drawing drawing = new Drawing();

                try
                {

                    URL url = new URL(targetURL);
                    URLConnection yc = url.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader( yc.getInputStream()));

                    String response = "";
                    String inputLine;
                    while ((inputLine = in.readLine()) != null)
                        response += inputLine;
                    in.close();

                    JSONObject obj = new JSONObject(response);

                    System.out.println(obj);

                    JSONArray jsonPoints = obj.getJSONArray("points");

                    int nbPts = jsonPoints.length();
                    for (int i = 0; i < nbPts; ++i) {
                        JSONObject point = jsonPoints.getJSONObject(i);
                        drawing.points.add(new Point(point.getInt("x"), point.getInt("y"), Color.RED, 7));
                    }

                    System.out.println(drawing.points);

                }
                catch(ClientProtocolException exception)
                {
                    System.out.println(exception.getMessage());

                }
                catch(IOException exception)
                {
                    System.out.println(exception.getMessage());
                } catch (Exception e) {
                    System.out.print(e);
                }

            }
        };

        thread.start();
    }*/


    public static List<Drawing> getMessages(final double lat, final double lng) {

        String url = "http://104.236.226.140/messages";
        List<Drawing> drawings = new ArrayList<>();

        try {

            JSONObject json = new JSONObject();

            json.put("lat", lat);
            json.put("lng", lng);

            String urlParameters = json.toString();

            String response = RequestManager.excutePost(url, urlParameters, true);
            System.out.println(response);

            JSONArray obj = new JSONArray(response);

            int nbDrawing = obj.length();
            for (int cntD = 0; cntD < nbDrawing; ++cntD) {

                // Object drawing
                Drawing drawing = new Drawing();

                JSONObject jsonDrawing = obj.getJSONObject(cntD);

                drawing.lat = jsonDrawing.getDouble("x");
                drawing.lng = jsonDrawing.getDouble("y");
                drawing.alt = 1000;//jsonDrawing.getDouble("z");
                drawing.yaw = jsonDrawing.getDouble("yaw");
                drawing.pitch = jsonDrawing.getDouble("pitch");
                drawing.roll = jsonDrawing.getDouble("roll");

                JSONArray jsonPoints = jsonDrawing.getJSONArray("points");

                int nbPts = jsonPoints.length();
                for (int i = 0; i < nbPts; ++i) {
                    JSONObject point = jsonPoints.getJSONObject(i);
                    drawing.points.add(new Point(point.getDouble("x"), point.getDouble("y"), Color.RED, 7));
                }

                drawings.add(drawing);

            }

        } catch (Exception e) {
            System.out.println(e);
        }

        return drawings;

    }

    @Override
    public void run() {
       // sendImage(mImage);
       // mImage = null;

    }
}
