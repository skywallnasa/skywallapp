package io.skywallapp.skywall;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class CameraActivity extends Activity implements Camera.PictureCallback, Camera.ShutterCallback, LocationListener, SensorEventListener
{
    private SensorManager mSensorManager;
    Sensor mAccelerometer;
    Sensor mMagnetometer;

    private TextView mIMUTextView;
    private CameraPreview mPreview;
    private RelativeLayout mLayout;

    LocationManager mlocManager;
    public double mLatitude = 0.0;
    public double mLongitude = 0.0;
    double mAzimuthAngle;
    double mPitchAngle;
    double mRollAngle;


    public void onPictureTaken(byte[] picture, Camera camera)
    {
        /*FileOutputStream outStream = null;
        try {
            // write to local sandbox file system
            // outStream =
            // CameraDemo.this.openFileOutput(String.format("%d.jpg",
            // System.currentTimeMillis()), 0);
            // Or write to sdcard
            File myDir = getFilesDir();
            String path = String.format("sdcard/%d.jpg", System.currentTimeMillis());
            outStream = new FileOutputStream(path);
            Log.d("YOYO", "onPictureTaken" + path);

            outStream.write(picture);
            outStream.close();
            Log.d("YOYO", "onPictureTaken - wrote bytes: " + picture.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
*/

        //Picture we can send (or we can use preview
        RequestManager manager = new RequestManager();
        manager.sendImage(picture,"http://104.236.226.140/upload", mLatitude, mLongitude, mAzimuthAngle, mPitchAngle, mRollAngle);
        //mCamera.startPreview();
        //manager.setData(picture);
        //manager.

        //String data =  RequestManager.excutePost("http://104.236.226.140/image", String urlParameters, "file")



    }

    public void onShutter()
    {
        //Save gyro, etc.
    }


    public void startGPS()
    {

        mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, this);

        if (mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
        {
             if(mLatitude >0)
            {
                mGPSLabel.setText("Lat: " + mLatitude + " Lng: " + mLongitude);
                mGPSLabel.setTextColor(Color.GREEN);
            }
            else
            {
                mGPSLabel.setText("GPS in progress...");
                mGPSLabel.setTextColor(Color.YELLOW);
            }
        }
        else
        {
            mGPSLabel.setText("GPS is off...");
            mGPSLabel.setTextColor(Color.RED);
        }
    }

    public int getFullscreenSystemUiVisibilityFlag()
    {
        int newUiOptions  = getWindow().getDecorView().getSystemUiVisibility();

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) //4.1
        {
            newUiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= 19) //4.4
        {
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            newUiOptions |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        return newUiOptions;
    }

    //Available for API 19 and up. API 16 and upward will have the status bar hidden.
    public void requestFullscreen()
    {

        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |=  WindowManager.LayoutParams.FLAG_FULLSCREEN;
        win.setAttributes(winParams);

        getWindow().getDecorView().setSystemUiVisibility(getFullscreenSystemUiVisibilityFlag());
    }

    TextView mGPSLabel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Hide status-bar
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Hide title-bar, must be before setContentView
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.activity_capture);
        mGPSLabel = (TextView)findViewById(R.id.gpsTextView);
        mGPSLabel.setText("GPS OFF");
        mIMUTextView = (TextView) findViewById(R.id.imuLabel);

        mLayout = (RelativeLayout)findViewById(R.id.captureLayout);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);


        mLayout.setGravity(RelativeLayout.CENTER_IN_PARENT);
        final CameraActivity context = this;
        ImageButton captureButton = (ImageButton)findViewById(R.id.shootBtn);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPreview.takePicture(context, context);
            }
        });

        setContentView(mLayout);
        startGPS();
        requestFullscreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Set the second argument by your choice.
        // Usually, 0 for back-facing camera, 1 for front-facing camera.
        // If the OS is pre-gingerbreak, this does not have any effect.
        startCamera();
        startIMU();
    }

    void startCamera()
    {
        mPreview = new CameraPreview(this, 0, CameraPreview.LayoutMode.FitToParent);
        LayoutParams previewLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        // Un-comment below lines to specify the size.
        //previewLayoutParams.height = 500;
        //previewLayoutParams.width = 500;

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        // Un-comment below line to specify the position.
        mPreview.setCenterPosition(width/2, height/2);
        mLayout.setBackgroundColor(Color.BLACK);
        mLayout.addView(mPreview, 0, previewLayoutParams);
    }

    void stopCamera()
    {
        mPreview.stop();
        mLayout.removeView(mPreview); // This is necessary.
        mPreview = null;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopIMU();
        stopCamera();
    }

    //GPS Location
    @Override
    public void onLocationChanged(Location loc)
    {
        mLatitude =loc.getLatitude();
        mLongitude =loc.getLongitude();

        mGPSLabel.setText("Lat: " + mLatitude + " Lng: " + mLongitude);
        mGPSLabel.setTextColor(Color.GREEN);
    }

    @Override
    public void onProviderDisabled(String provider)
    {
        mGPSLabel.setText("GPS is off...");
        mGPSLabel.setTextColor(Color.RED);
    }
    @Override
    public void onProviderEnabled(String provider)
    {
        mGPSLabel.setText("GPS in progress...");
        mGPSLabel.setTextColor(Color.YELLOW);
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
    }



    public void startIMU()
    {
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    public void stopIMU()
    {
        mSensorManager.unregisterListener(this);
    }

    //IMU
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
        // Do something here if sensor accuracy changes.
        // You must implement this callback in your code.
    }

    float[] mGravity = null;
    float[] mGeomagnetic = null;


    String format(double number)
    {
        return String.format("%.2f", number) ;
    }

    float[] inR = new float[16];
    float[] outR = new float[16];

    float[] I = new float[16];
    float[] gravity = new float[3];
    float[] geomag = new float[3];
    float[] orientVals = new float[3];

    double azimuth = 0;
    double pitch = 0;
    double roll = 0;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        if (sensorEvent.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)
            return;

        // Gets the value of the sensor that has been changed
        switch (sensorEvent.sensor.getType())
        {
            case Sensor.TYPE_ACCELEROMETER:
                gravity[0]=(gravity[0]*2+sensorEvent.values[0])*0.33334f;
                gravity[1]=(gravity[1]*2+sensorEvent.values[1])*0.33334f;
                gravity[2]=(gravity[2]*2+sensorEvent.values[2])*0.33334f;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                geomag[0]=(geomag[0]*1+sensorEvent.values[0])*0.5f;
                geomag[1]=(geomag[1]*1+sensorEvent.values[1])*0.5f;
                geomag[2]=(geomag[2]*1+sensorEvent.values[2])*0.5f;
                break;

        }


        /*

           if (type == Sensor.TYPE_MAGNETIC_FIELD) {
      geomag[0]=(geomag[0]*1+evt.values[0])*0.5f;
      geomag[1]=(geomag[1]*1+evt.values[1])*0.5f;
      geomag[2]=(geomag[2]*1+evt.values[2])*0.5f;
    } else if (type == Sensor.TYPE_ACCELEROMETER) {
      gravity[0]=(gravity[0]*2+evt.values[0])*0.33334f;
      gravity[1]=(gravity[1]*2+evt.values[1])*0.33334f;
      gravity[2]=(gravity[2]*2+evt.values[2])*0.33334f;
    }
         */

        // If gravity and geomag have values then find rotation matrix
        if (gravity != null && geomag != null) {

            // checks that the rotation matrix is found
            boolean success = SensorManager.getRotationMatrix(inR, I,
                    gravity, geomag);
            if (success) {
                SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_X, SensorManager.AXIS_Z, outR);

                SensorManager.getOrientation(outR, orientVals);
                mAzimuthAngle = Math.toDegrees(orientVals[0]);
                mPitchAngle = -Math.toDegrees(orientVals[1]);
                mRollAngle = Math.toDegrees(orientVals[2]);
                mIMUTextView.setText("Azimuth:"+format(mAzimuthAngle)+"\nPitch: "+format(mPitchAngle)+"\nRoll: "+format(mRollAngle));

            }
        }
    }
        /*
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null)
        {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                mAzimuthAngle = Math.toDegrees(-orientation[0]); // orientation contains: azimut, pitch and roll
                mPitchAngle = Math.toDegrees(orientation[1]);
                mRollAngle = Math.toDegrees(orientation[2]);
                mIMUTextView.setText("Azimuth:"+format(mAzimuthAngle)+"\nPitch: "+format(mPitchAngle)+"\nRoll: "+format(mRollAngle));
            }
        }
    }*/

}
