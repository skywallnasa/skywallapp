package io.skywallapp.skywall;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import io.skywallapp.skywall.utils.ActivityHelpers;

public class Main extends Activity
{

    Button mCaptureBtn;
    Button mDrawingBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        final Context ctx = this;

        mCaptureBtn = (Button) findViewById(R.id.debugCaptureBtn);
        mCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ctx, CameraActivity.class);
                startActivity(myIntent);

            }
        });
        mDrawingBtn = (Button) findViewById(R.id.debug2dBtn);
    mDrawingBtn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent myIntent = new Intent(ctx, DrawingActivity.class);
            startActivity(myIntent);
            ActivityHelpers.setUiFullscreen(getWindow());

        }
    });


        ImageButton startBtn = (ImageButton) findViewById(R.id.startButton);
        startBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ctx, ArCameraActivity.class);
myIntent.putExtra(DrawingActivity.VALID_DRAWING, false);
                startActivity(myIntent);
            }
        });



        Button debugARFrameworkBtn = (Button)  findViewById(R.id.debugARFrameworkBtn);
        debugARFrameworkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ctx, ArCameraActivity.class);
myIntent.putExtra(DrawingActivity.VALID_DRAWING, false);
                startActivity(myIntent);
            }
        });

        ActivityHelpers.setUiFullscreen(getWindow());


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

}
