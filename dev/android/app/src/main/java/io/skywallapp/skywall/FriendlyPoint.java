package io.skywallapp.skywall;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by vonziper on 15-04-11.
 */
public class FriendlyPoint extends Point {

    private final Point neighbour;

    public FriendlyPoint(final double x, final double y, final int col, final Point neighbour, final int width) {
        super(x, y, col, width);
        this.neighbour = neighbour;
    }

    @Override
    public void draw(final Canvas canvas, final Paint paint) {
        paint.setColor(color);
        paint.setStrokeWidth(size);
        canvas.drawLine((float)x,(float)y, (float)neighbour.x, (float)neighbour.y, paint);
        // experimenting with smoother drawing --
        // the circle should hopefully act as a smoothing hinge between line segments
        // o----o----o------o
        canvas.drawCircle((float)x, (float)y, size/2, paint);
    }

    @Override
    public String toString() {
        return x + ", " + y + ", " + color + "; N[" + neighbour.toString() + "]";
    }
}
