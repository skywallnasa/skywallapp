package io.skywallapp.skywall;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vonziper on 15-04-11.
 */
public class Point {
    public final double x, y;
    protected final int color;
    protected final int size;

    public Point(final double x, final double y, final int color, final int width) {
        this.x = x;
        this.y = y;
        this.color = Color.RED;
        this.size = width;
    }

    public void draw(final Canvas canvas, final Paint paint) {
        paint.setColor(color);
        canvas.drawCircle((float)x, (float)y, size/2, paint);
    }

    public JSONObject toJson() throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("x",x);
        obj.put("y",x);
        return obj;
    }

    @Override
    public String toString() {
        return x + ", " + y;
    }
}
