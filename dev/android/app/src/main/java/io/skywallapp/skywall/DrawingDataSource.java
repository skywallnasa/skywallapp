package io.skywallapp.skywall;

import android.content.res.Resources;
import android.graphics.Color;
import android.util.Log;

import com.jwetherell.augmented_reality.ui.Marker;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jpjodoin on 15-04-12.
 */

public class DrawingDataSource {

    NetworkDrawingListener mListener = null;
    public boolean mInit = false;
    List<Drawing> mDrawings = null;
    private Resources mRessources;
    public DrawingDataSource(Resources res)
    {
        mRessources = res;
        //Run thread
    }

    public void registerNetworkListener(NetworkDrawingListener listener)
    {
        mListener = listener;
    }


    public void fetch(final double lat, final double lng)
    {
        mInit = true;
        Thread thread = new Thread()
        {
            @Override
            public void run()
            {



                mDrawings = RequestManager.getMessages(lat, lng);
                if(mListener != null)
                    mListener.onFetchDone();


                Log.d("SendThread", "End");

            }
        };

        thread.start();
    }

    public List<Marker> getMarkers()
    {
        List<Marker> markerList = new ArrayList<Marker>();

        if(mDrawings != null)
        {
            System.out.println("From SERVER " + mDrawings.size() );

            for(Drawing drawing: mDrawings)
            {
                System.out.println("Lat:" + drawing.lat + " Lng:" + drawing.lng + " Alt:" + drawing.alt);
                markerList.add(new DrawingMarker("Test", drawing.lat, drawing.lng, drawing.alt, Color.BLUE, drawing));
            }

        }
        return markerList;
    }
}
