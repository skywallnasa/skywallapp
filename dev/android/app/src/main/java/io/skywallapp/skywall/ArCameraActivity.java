package io.skywallapp.skywall;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.SensorEvent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jwetherell.augmented_reality.activity.AugmentedReality;
import com.jwetherell.augmented_reality.data.ARData;
import com.jwetherell.augmented_reality.data.TestDataSource;

import io.skywallapp.skywall.utils.ActivityHelpers;

/**
 * This class extends the AugmentedReality and is designed to be an example on
 * how to extends the AugmentedReality class to show multiple data sources.
 * 
 * @author Justin Wetherell <phishman3579@gmail.com>
 */
public class ArCameraActivity extends AugmentedReality implements Camera.PictureCallback, Camera.ShutterCallback, NetworkDrawingListener {


    private Drawing mCurrentDrawing = null;
    private TextView mIMUTextView;
    private TextView mGPSLabel;


    public void onFetchDone()
    {
        runOnUiThread(new Thread(new Runnable() {
            public void run() {
               ARData.addMarkers(mSource.getMarkers());
        }}));
    }


    // private static Toast myToast = null;
   // private static VerticalTextView text = null;

    boolean isInPositionDrawingMode()
    {
        return mCurrentDrawing != null;
    }

    DrawingDataSource mSource = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();

        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.activity_capture, null);
        RelativeLayout.LayoutParams layoutParamsControl = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        this.addContentView(layout, layoutParamsControl);

        mGPSLabel = (TextView)findViewById(R.id.gpsTextView);
        mGPSLabel.setText("GPS OFF");
        mIMUTextView = (TextView) findViewById(R.id.imuLabel);


        final ArCameraActivity context = this;
        ImageButton captureButton = (ImageButton)findViewById(R.id.shootBtn);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isInPositionDrawingMode())
                {
                    mCurrentDrawing.lat = mLatitude;
                    mCurrentDrawing.lng = mLongitude;
                    mCurrentDrawing.pitch = mPitchAngle;
                    mCurrentDrawing.yaw = mAzimuthAngle;
                    mCurrentDrawing.roll = mRollAngle;
                    mCurrentDrawing.sendToServer();

                    Intent myIntent = new Intent(context, ShareActivity.class);
                    startActivity(myIntent);
                }
                else
                {
                    getCamera().takePicture(context, null, context);
                }
            }
        });

        if(bundle.getBoolean(DrawingActivity.VALID_DRAWING))
        {
            mCurrentDrawing = DrawingActivity.mCurrentDrawing;
            captureButton.setBackgroundResource(R.drawable.done);
        }
        else
        {
            captureButton.setBackgroundResource(R.drawable.paint);
        }


        mSource = new DrawingDataSource(this.getResources());
        mSource.registerNetworkListener(this);
       //TODO: fetch current points from server
        TestDataSource test = new TestDataSource(this.getResources());
        ARData.addMarkers(test.getMarkers());
        ActivityHelpers.setUiFullscreen(getWindow());
    }

    @Override
    public void onPostDraw2D(Canvas canvas)
    {
        if(isInPositionDrawingMode() && mCurrentDrawing!= null)
        {
            mCurrentDrawing.onDraw(canvas);
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mSource.registerNetworkListener(null); //Unregister
    }

    /**
     * {@inheritDoc}
     */
   /* @Override
    protected void markerTouched(Marker marker) {
        text.setText(marker.getName());
        myToast.show();
    }*/


    public void onPictureTaken(byte[] picture, Camera camera)
    {
        //Picture we can send (or we can use preview
        RequestManager manager = new RequestManager();
        manager.sendImage(picture,"http://104.236.226.140/upload", mLatitude, mLongitude, mAzimuthAngle, mPitchAngle, mRollAngle);
        //TODO: enregistrer image ?
        Intent myIntent = new Intent(this, DrawingActivity.class);
        startActivity(myIntent);
     }

    @Override
    public void onLocationChanged(Location loc)
    {
        super.onLocationChanged(loc);
        mLatitude =loc.getLatitude();
        mLongitude =loc.getLongitude();

        if(mSource.mInit == false)
        mSource.fetch(mLatitude, mLongitude);


        mGPSLabel.setText("Lat: " + mLatitude + " Lng: " + mLongitude);
        mGPSLabel.setTextColor(Color.GREEN);
    }

    @Override
    public void onProviderDisabled(String provider)
    {
        super.onProviderDisabled(provider);
        mGPSLabel.setText("GPS is off...");
        mGPSLabel.setTextColor(Color.RED);
    }
    @Override
    public void onProviderEnabled(String provider)
    {
        super.onProviderEnabled(provider);
        mGPSLabel.setText("GPS in progress...");
        mGPSLabel.setTextColor(Color.YELLOW);
    }
    String format(double number)
    {
        return String.format("%.2f", number) ;
    }

    @Override
    public void onSensorChanged(SensorEvent evt) {
        super.onSensorChanged(evt);
        mIMUTextView.setText("Azimuth:"+format(mAzimuthAngle)+"\nPitch: "+format(mPitchAngle)+"\nRoll: "+format(mRollAngle));
    }

    public void onShutter()
    {
        //Save gyro, etc.
    }


}
