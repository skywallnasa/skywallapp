package io.skywallapp.skywall;

import android.graphics.Canvas;

/**
 * Created by jpjodoin on 15-04-12.
 */
public interface Draw2DListener
{
    public void onPostDraw2D(Canvas canvas);
}
