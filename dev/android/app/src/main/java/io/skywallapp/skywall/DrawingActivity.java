package io.skywallapp.skywall;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import io.skywallapp.skywall.utils.ActivityHelpers;

public class DrawingActivity extends ActionBarActivity {

    DrawView drawView;
    RelativeLayout mLayout;
    RequestManager requestManager;

    static Drawing mCurrentDrawing = null;

    public static String VALID_DRAWING = "VALID_DRAWING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        requestManager = new RequestManager();
        mLayout = new RelativeLayout(this);
        mLayout.setGravity(RelativeLayout.CENTER_IN_PARENT);

        Button sendButton = new Button(this);
        Button getButton = new Button(this);


        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                drawView.drawing.sendToServer();
                System.out.println("Send data to server");
            }
        });

        getButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestManager.getMessages(0,0);
                System.out.println("Get data from server");
            }
        });

        sendButton.setText("Send");
        getButton.setText("Get");

        drawView = new DrawView(this);
        drawView.setBackgroundColor(getResources().getColor(R.color.space_blue));
        drawView.requestFocus();

        ImageButton placePoints = new ImageButton(this);
        placePoints.setBackgroundResource(R.drawable.insert);
        final DrawingActivity ctx = this;
        placePoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mCurrentDrawing = drawView.getDrawing();
                mCurrentDrawing.width = drawView.getWidth();
                mCurrentDrawing.height = drawView.getHeight();

                //Set points in bundle
                Intent myIntent = new Intent(ctx, ArCameraActivity.class);
                myIntent.putExtra(VALID_DRAWING, true);
                startActivity(myIntent);
            }
        });

        LinearLayout drawngLayout = new LinearLayout(this);
        //drawngLayout.addView(sendButton);
        //drawngLayout.addView(getButton);
        drawngLayout.addView(placePoints);

        drawngLayout.setGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);

        mLayout.addView(drawView);
        mLayout.addView(drawngLayout, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        setContentView(mLayout);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
	ActivityHelpers.setUiFullscreen(getWindow());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drawing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
