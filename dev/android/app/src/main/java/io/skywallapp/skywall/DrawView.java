package io.skywallapp.skywall;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by vonziper on 15-04-11.
 */
public class DrawView extends View implements View.OnTouchListener {

    Drawing drawing = new Drawing();
    Paint paint = new Paint();
    int drawColor;
    int pointSize;

    Drawing getDrawing() { return drawing;}

    public DrawView(Context context) {
        super(context);

        // default point color
        drawColor = Color.WHITE;
        // default size color
        pointSize = 7;

        setFocusable(true);
        setFocusableInTouchMode(true);

        this.setOnTouchListener(this);

        paint.setAntiAlias(true);

    }

    public void setPoints(List<Point> points) {
        this.drawing.points = points;
    }


    // Clear the canvas
    public void clearPoints () {
        drawing.points.clear();
        forceRedraw();
    }

    /**
     * Force view to redraw. Without this points aren't cleared until next action
     */
    public void forceRedraw() {
        invalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {
        // for each point, draw on canvas
        for (Point point : drawing.points) {
            point.draw(canvas, paint);
        }
    }

    public boolean onTouch(View view, MotionEvent event) {
        int color = Color.BLACK;

        Point point;
        if(event.getAction() == MotionEvent.ACTION_MOVE) {
            point = new FriendlyPoint(event.getX(), event.getY(), color, drawing.points.get(drawing.points.size() - 1), pointSize);
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            point = new Point(event.getX(), event.getY(), color, pointSize);
        } else {
            return false;
        }
        drawing.points.add(point);
        forceRedraw();
        return true;
    }


}