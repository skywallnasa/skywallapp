package io.skywallapp.skywall;

/**
 * Created by jpjodoin on 15-04-11.
 */
public class Size
{
    public Size(int _width, int _height)
    {
        width = _width;
        height = _height;
    }

    public int width;
    public int height;
}
