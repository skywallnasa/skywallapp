package io.skywallapp.skywall;

import android.graphics.Canvas;

import com.jwetherell.augmented_reality.ui.Marker;

/**
 * This class extends Marker and draws an icon instead of a circle for it's
 * visual representation.
 * 
 * @author Justin Wetherell <phishman3579@gmail.com>
 */
public class DrawingMarker extends Marker {

    private Drawing mDrawing = null;

    public DrawingMarker(String name, double latitude, double longitude, double altitude, int color, Drawing drawing) {
        super(name, latitude, longitude, altitude, color);
        mDrawing = drawing;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void drawIcon(Canvas canvas) {
  /*      if (canvas == null || bitmap == null) throw new NullPointerException();

        // gpsSymbol is defined in Marker
        int currentWidth = bitmap.getWidth();
        int currentHeight = bitmap.getHeight();
        float ratio = (float)currentWidth/(float)currentHeight;
        int baseWidth = 512;
        if (gpsSymbol == null) gpsSymbol = new PaintableIcon(bitmap, baseWidth, (int)(baseWidth/ratio));
        super.drawIcon(canvas);*/
        mDrawing.onDraw(canvas);
    }
}
