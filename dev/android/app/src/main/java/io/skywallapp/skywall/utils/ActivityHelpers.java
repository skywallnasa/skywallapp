package io.skywallapp.skywall.utils;

import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by jpjodoin on 15-04-12.
 */
public class ActivityHelpers
{
    static public void setUiFullscreen(Window win)
    {
        requestFullscreen(win);
    }

    static private int getFullscreenSystemUiVisibilityFlag(Window win)
    {
        int newUiOptions  = win.getDecorView().getSystemUiVisibility();

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) //4.1
        {
            newUiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }
        if (Build.VERSION.SDK_INT >= 19) //4.4
        {
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            newUiOptions |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        return newUiOptions;
    }

    //Available for API 19 and up. API 16 and upward will have the status bar hidden.
    static private void requestFullscreen(Window win)
    {

        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.flags |=  WindowManager.LayoutParams.FLAG_FULLSCREEN;
        win.setAttributes(winParams);
        win.getDecorView().setSystemUiVisibility(getFullscreenSystemUiVisibilityFlag(win));
    }
}
