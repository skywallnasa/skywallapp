package com.jwetherell.augmented_reality.data;

import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import com.jwetherell.augmented_reality.ui.IconMarker;
import com.jwetherell.augmented_reality.ui.Marker;

import java.util.ArrayList;
import java.util.List;

import io.skywallapp.skywall.R;

/**
 * Created by jpjodoin on 15-04-12.
 */
public class TestDataSource extends DataSource
{
    private Resources mRessources;
    public TestDataSource(Resources res)
    {
        mRessources = res;
    }

    public List<Marker> getMarkers()
    {
        List<Marker> markerList = new ArrayList<Marker>();
        //markerList.add(new IconMarker("Test3", 45.5590481,-73.5530133, 0, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.done)));
       // markerList.add(new IconMarker("Test3", 45.5590483,-73.5530135, 0, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.done)));
        markerList.add(new IconMarker("Test3", 45.2363748,-73.6587913, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.logo)));
        markerList.add(new IconMarker("Test", 45.3363348,-73.7587713, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.a)));
        markerList.add(new IconMarker("Test2", 45.4363748,-73.697313, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.b)));
        markerList.add(new IconMarker("Test4", 45.5563548,-73.6285113, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.c)));
        markerList.add(new IconMarker("Test5", 45.2763242,-73.3487913, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.d)));
        markerList.add(new IconMarker("Test6", 45.4663148,-73.1186113, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.e)));
        markerList.add(new IconMarker("Test", 45.3363348,-73.8587713, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.f)));
        markerList.add(new IconMarker("Test", 45.4563348,-73.8187713, 1000, Color.BLUE,  BitmapFactory.decodeResource(mRessources, R.drawable.g)));

        //markerList.add(new Marker("Marker",  45.5590475,-73.5530129, 0, Color.YELLOW));



        return markerList;
    }

}
